use futures::prelude::*;
use influxdb2::models::DataPoint;
use influxdb2::Client;
use log::{debug, error, info};
use rumqttc::Event;
use rumqttc::{AsyncClient, Incoming, MqttOptions, Publish, QoS};
use serde::Deserialize;
use serde_json::json;
use std::fs::File;
use std::io::Read;
use std::sync::Arc;
use std::time::{SystemTime, UNIX_EPOCH};
use tokio::sync::Mutex;
use tokio::time::Duration;
use tokio::{task, time};

static SOLAR_POWER_YIELD: &str = "N/e45f01859af0/solarcharger/288/Yield/Power";
static SOLAR_POWER_YIELD_TODAY: &str = "N/e45f01859af0/solarcharger/288/History/Daily/0/Yield";
static SOLAR_POWER_YIELD_TODAY_KEEP_ALIVE: &str = "solarcharger/288/History/Daily/0/Yield";
static SOLAR_POWER_YIELD_YESTERDAY: &str = "N/e45f01859af0/solarcharger/288/History/Daily/1/Yield";
static SOLAR_POWER_YIELD_YESTERDAY_KEEP_ALIVE: &str = "solarcharger/288/History/Daily/1/Yield";
static BATTERY_CONSUMED_AH: &str = "N/e45f01859af0/system/0/Dc/Battery/ConsumedAmphours";
static BATTERY_CONSUMED_AH_KEEP_ALIVE: &str = "system/0/Dc/Battery/ConsumedAmphours";
static BATTERY_POWER: &str = "N/e45f01859af0/battery/289/Dc/0/Power";
static BATTERY_POWER_KEEP_ALIVE: &str = "battery/289/Dc/0/Power";
static BATTERY_TOTAL_DISCHARGED: &str = "N/e45f01859af0/battery/289/History/DischargedEnergy";
static BATTERY_TOTAL_DISCHARGED_ALIVE: &str = "battery/289/History/DischargedEnergy";
static BATTERY_TOTAL_CHARGED: &str = "N/e45f01859af0/battery/289/History/ChargedEnergy";
static BATTERY_TOTAL_CHARGED_ALIVE: &str = "battery/289/History/ChargedEnergy";
static SYSTEM_POWER: &str = "N/e45f01859af0/system/0/Dc/System/Power";
static SYSTEM_POWER_KEEP_ALIVE: &str = "system/0/Dc/System/Power";
static BATTERY_VOLTAGE: &str = "N/e45f01859af0/system/0/Dc/Battery/Voltage";
static AMP_HOURS_USED: &str = "N/e45f01859af0/battery/289/History/TotalAhDrawn";

#[tokio::main]
async fn main() {
    env_logger::init();

    let token_file = match std::env::var("TOKEN_FILE") {
        Ok(value) => value,
        Err(_) => panic!("Failed to get token file exiting"),
    };
    info!("token-file is  {}", token_file);

    let mut file = File::open(token_file).unwrap();
    let mut token: String = "".to_string();
    file.read_to_string(&mut token).unwrap();

    token = token.trim().to_string();
    info!("token retrieved as {}", token);

    // let host = std::env::var("INFLUXDB_HOST").unwrap();
    let host = env!("INFLUXDB_HOST");
    // let org = std::env::var("INFLUXDB_ORG").unwrap();
    let org = env!("INFLUXDB_ORG");
    let influx_client = Arc::new(Mutex::new(Client::new(host, org, &token)));

    'main: loop {
        let mut mqttoptions = MqttOptions::new(env!("MQTT_ANNOUNCE_NAME"), env!("MQTT_HOST"), 1883);
        mqttoptions.set_keep_alive(Duration::from_secs(5));

        let (client, mut eventloop) = AsyncClient::new(mqttoptions, 10);
        client
            .subscribe(
                SOLAR_POWER_YIELD,
                QoS::AtMostOnce,
            )
            .await
            .unwrap();
        client
            .subscribe(
                BATTERY_VOLTAGE,
                QoS::AtMostOnce,
            )
            .await
            .unwrap();
        client
            .subscribe(
                AMP_HOURS_USED,
                QoS::AtMostOnce,
            )
            .await
            .unwrap();
        client
            .subscribe(
                SOLAR_POWER_YIELD_TODAY,
                QoS::AtMostOnce,
            )
            .await
            .unwrap();
        client
            .subscribe(
                SOLAR_POWER_YIELD_YESTERDAY,
                QoS::AtMostOnce,
            )
            .await
            .unwrap();
        client
            .subscribe(
                BATTERY_CONSUMED_AH,
                QoS::AtMostOnce,
            )
            .await
            .unwrap();

        client
            .subscribe(
                SYSTEM_POWER,
                QoS::AtMostOnce,
            )
            .await
            .unwrap();
        client
            .subscribe(
                BATTERY_POWER,
                QoS::AtMostOnce,
            )
            .await
            .unwrap();
        client
            .subscribe(
                BATTERY_TOTAL_CHARGED,
                QoS::AtMostOnce,
            )
            .await
            .unwrap();
        client
            .subscribe(
                BATTERY_TOTAL_DISCHARGED,
                QoS::AtMostOnce,
            )
            .await
            .unwrap();

        // client.publish("R/e45f01859af0/keepalive", QoS::AtLeastOnce, false, json!(["solarcharger/+/Yield/Power"]).to_string().into_bytes() );
        let _keep_alive_loop = task::spawn(async move {
            let mut interval = time::interval(Duration::new(30, 0));

            loop {
                interval.tick().await;
                info!("sent packet");

                let _ = client
                    .publish(
                        "R/e45f01859af0/keepalive",
                        QoS::AtLeastOnce,
                        false,
                        json!(["solarcharger/+/Yield/Power",
                            "system/0/Dc/Battery/Voltage",
                            "battery/289/History/TotalAhDrawn",
                            SOLAR_POWER_YIELD_TODAY_KEEP_ALIVE,
                            SOLAR_POWER_YIELD_YESTERDAY_KEEP_ALIVE,
                        BATTERY_CONSUMED_AH_KEEP_ALIVE,
                        BATTERY_POWER_KEEP_ALIVE,
SYSTEM_POWER_KEEP_ALIVE,
                            BATTERY_TOTAL_CHARGED_ALIVE,
                            BATTERY_TOTAL_DISCHARGED_ALIVE,

                        ])
                            .to_string()
                            .into_bytes(),
                    )
                    .await;
            }
        });

        // keep_alive_loops.await;

        loop {
            let notification = match eventloop.poll().await {
                Ok(notifaction) => notifaction,
                Err(err) => {
                    dbg!(err);
                    error!("connection to mqtt lost attempting to reconnect");
                    continue 'main;
                }
            };

            match notification {
                Event::Incoming(packet) => match packet {
                    Incoming::Connect(_) => {}
                    Incoming::ConnAck(_) => {}
                    Incoming::Publish(publish) => {
                        debug!("Get timestamp");
                        let Some(time_received) = SystemTime::now()
                            .duration_since(UNIX_EPOCH).ok()
                            else {
                                error!("Unable to compute time stamp");
                                error!("nothing sent to influx");
                                return;
                            };

                        debug!("convert timestamp to correct type");
                        let Some(time_received): Option<i64> = time_received.as_nanos().try_into().ok() else {
                            error!("unable to convert timestamp into nanos");
                            error!("nothing sent to influx");
                            return;
                        };

                        debug!("recived {}, from {}", std::str::from_utf8(&publish.payload).unwrap(), publish.topic);


                        if publish.topic == BATTERY_VOLTAGE{
                            let c_influx_client = influx_client.clone();
                            task::spawn(async move {
                                handle_publish(publish, c_influx_client, time_received, "battery", "current", "V")
                                    .await;
                            });
                        } else if publish.topic == SOLAR_POWER_YIELD {
                            let c_influx_client = influx_client.clone();
                            task::spawn(async move {
                                handle_publish(publish, c_influx_client, time_received, "solarcharger", "current", "W").await;
                            });
                        }else if publish.topic == AMP_HOURS_USED {
                            let c_influx_client = influx_client.clone();
                            task::spawn(async move {
                                handle_publish(publish, c_influx_client, time_received, "battery", "history", "DrawnAh").await;
                            });
                        }else if publish.topic == SOLAR_POWER_YIELD_TODAY {
                            let c_influx_client = influx_client.clone();
                            task::spawn(async move {
                                handle_publish(publish, c_influx_client, time_received, "solarcharger", "today", "Kw").await;
                            });
                        }else if publish.topic == SOLAR_POWER_YIELD_YESTERDAY {
                            let c_influx_client = influx_client.clone();
                            task::spawn(async move {
                                handle_publish(publish, c_influx_client, time_received, "solarcharger", "yesterday", "Kw").await;
                            });
                        } else if publish.topic == BATTERY_CONSUMED_AH {
                            let c_influx_client = influx_client.clone();
                            task::spawn(async move {
                                handle_publish(publish, c_influx_client, time_received, "battery", "history", "ConsumedAh").await;
                            });
                        }else if publish.topic == SYSTEM_POWER{
                            let c_influx_client = influx_client.clone();
                            task::spawn(async move {
                                handle_publish(publish, c_influx_client, time_received, "system", "current", "W").await;
                            });
                        }else if publish.topic == BATTERY_POWER {
                            let c_influx_client = influx_client.clone();
                            task::spawn(async move {
                                handle_publish(publish, c_influx_client, time_received, "battery", "current", "W").await;
                            });
                        }else if publish.topic == BATTERY_TOTAL_DISCHARGED {
                            let c_influx_client = influx_client.clone();
                            task::spawn(async move {
                                handle_publish(publish, c_influx_client, time_received, "battery", "history", "DischargedKw").await;
                            });
                        }else if publish.topic == BATTERY_TOTAL_CHARGED {
                            let c_influx_client = influx_client.clone();
                            task::spawn(async move {
                                handle_publish(publish, c_influx_client, time_received, "battery", "history", "ChargedKw").await;
                            });
                        }
                    }
                    Incoming::PubAck(_) => {}
                    Incoming::PubRec(_) => {}
                    Incoming::PubRel(_) => {}
                    Incoming::PubComp(_) => {}
                    Incoming::Subscribe(_) => {}
                    Incoming::SubAck(_) => {}
                    Incoming::Unsubscribe(_) => {}
                    Incoming::UnsubAck(_) => {}
                    Incoming::PingReq => {}
                    Incoming::PingResp => {}
                    Incoming::Disconnect => {}
                },
                Event::Outgoing(_packet) => {}
            }
        }
    }
}

async fn handle_publish(publish: Publish, client: Arc<Mutex<Client>>, time_received: i64, mesurement: &str, topic: &str, field: &str) {
    debug!("Deserialize message into struct");
    #[derive(Deserialize, Debug)]
    struct Message {
        value: f64,
    }

    let Some(parsed): Option<Message> = serde_json::from_str(std::str::from_utf8(&publish.payload).unwrap()).ok() else {
        error!("Failed to deserialise this payload: {:?}", &publish.payload);
        error!("nothing sent to influx");
        return;
    };

    debug!("Construct influx data");
    let Some(point) = DataPoint::builder(mesurement)
        .tag("topic", topic)
        .field(field, parsed.value)
        .timestamp(time_received)
        .build().ok() else {
        error!("Failed to construct influx data");
        error!("nothing sent to influx");
        return;
    };

    match client
        .lock()
        .await
        .write(env!("INFLUXDB_BUCKET"), stream::iter(vec![point]))
        .await
    {
        Ok(_) => {}
        Err(err) => {
            error!("failed to send to influx, {}", err);
        }
    }
}



